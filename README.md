# tcl-lang.org CSS

This CSS style sheet makes [www.tcl-lang.org](https://www.tcl-lang.org/) responsive with minimal changes to the old table-based markup.
To achieve this, it uses very specific CSS selectors (which will break if the markup changes), media queries, and Flexbox.

## Building on Debian/Ubuntu
```sh
sudo apt install make minify tcl
make
```

You will need to add the following tag to the `<head>` of your documents.

```html
<meta name="viewport" content="width=device-width, initial-scale=1">
```

## License

MIT.
See [LICENSE](LICENSE) and [AUTHORS](AUTHORS).

[normalize.css](https://github.com/necolas/normalize.css) is copyright Nicolas
Gallagher and Jonathan Neal and distributed under the MIT license.
See
[vendor/normalize.css/LICENSE.md](vendor/normalize.css/LICENSE.md).
