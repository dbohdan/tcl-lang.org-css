#! /usr/bin/env tclsh
# This script generates devsite.css.
# Copyright (c) 2020-2021, 2023-2024 D. Bohdan.
# License: MIT

package require Tcl 8.6 9

proc gen {} {
    set bgWhite #fff
    set bgGray #ddd

    set blueGray #ddf
    set deepBlueGray #bbf
    set lightBlueGray #eef

    set darkGray #8d8d8d
    set gray #ccc
    set lightGray #aaa
    set lightText #444
    set tableAltGray #eee

    set borderWidth 2px

    set mobileWidth 65
    set onDesktop "@media (min-width: ${mobileWidth}rem)"
    set onMobile "@media (max-width: [expr {$mobileWidth - 0.001}]rem)"

    set banners {.banner, .bannerlong}

    set logosA [string trim {
        body
        > table:first-of-type
        > tbody:first-of-type
        > tr:first-of-type
        > td:first-of-type
        > a:first-of-type
    }]

    # The table with the search form and optionally `p.banner` (the page
    # title).
    set searchBannerTable {
        body
        > table:nth-of-type(2)
    }

    unindent [subst -nobackslashes -nocommands {
        /*! www.tcl-lang.org CSS | MIT License |\
            gitlab.com/dbohdan/tcl-lang.org-css */

        /* What follows is one big hack to make the website responsive without
         * touching the table layout. */


        /* The basics. */
        html {
            margin: 0 auto;
            max-width: 70rem;
            background-color: $bgGray;
            box-sizing: border-box;
            font-size: 16px;
        }

        *, *::before, *::after {
            box-sizing: border-box;
        }

        body {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 1.5rem;
            padding-left: 1.5rem;
            padding-right: 1.5rem;
        }

        body > * {
            width: 100%;
        }

        /* Hide old decorative elements. */
        body > div:empty {
            display: none;
        }
        body > br {
            display: none;
        }

        a:not(:hover) {
            text-decoration: none;
        }

        .biglink::before {
            content: "\27a4";
        }

        pre, pre > * {
            max-width: 85vw;
            overflow: auto;
        }
        $onMobile {
            pre, pre > * {
                white-space: pre-wrap;
            }
        }


        /* The logos at the top of the page. */
        /* Hide "Hosted by ActiveState".  The website no longer is. */
        body
        > table:first-of-type
        > tbody:first-of-type
        > tr:first-of-type
        > td:nth-of-type(2)
        {
            display: none;
        }

        /* The feather logo. */
        $logosA
        > img:first-of-type
        {
            float: right;
        }

        /* The "Tcl Developer Xchange" logo. */
        $logosA
        > img:nth-of-type(2)
        {
            float: left;
        }
        $onMobile {
            /* Scale "TDX". */
            $logosA
            > img:nth-of-type(2)
            {
                max-width: 65vw;
                height: auto;
                padding-top: 0.5rem;
            }

            /* Hide the feather. */
            $logosA
            > img:first-of-type
            {
                display: none;
            }
        }

        /* Global navigation. */
        #globalnav ul {
            margin: 0;
            overflow: hidden;
            padding: 0;
            list-style: none;
        }
        #globalnav a {
            display: block;
            color: $lightText;
            font-size: 75%;
            font-weight: bold;
            background: $blueGray;
            margin: 0;
            padding: .25rem 1rem;
        }
        #globalnav a:hover, #globalnav a:active {
            background: $lightBlueGray;
            text-decoration: none;
        }
        #globalnav a.here:link, #globalnav a.here:visited {
            background: $deepBlueGray;
        }
        body > table:first-of-type {
            padding-top: 0.25rem;
            width: 100%;
        }
        $onDesktop {
            #globalnav {
                padding-top: 1rem;
                padding-bottom: 1.5rem;
            }
            #globalnav ul {
                display: flex;
                flex-direction: row;
                border: solid $borderWidth $lightGray;
            }
            #globalnav li {
                flex-grow: 1;
                margin: 0;
                padding: 0;
            }
            #globalnav li a {
                border-left: solid $borderWidth $lightGray;
                text-align: center;
            }
            #globalnav li:first-of-type a {
                border-left: 0;
            }
        }
        $onMobile {
            #globalnav {
                border: solid $borderWidth $lightGray;
                margin-top: 0.5rem;
                margin-bottom: 1rem;
            }
        }

        $searchBannerTable
        > tbody
        > tr:first-of-type {
            vertical-align: middle;
        }

        $searchBannerTable
        form {
            margin-bottom: 1rem;
        }


        /* The search form. */
        input[type=text] {
            border: solid $borderWidth $lightGray;
            background-color: $bgWhite;
        }
        img[src$="Search.gif"] {
            position: relative;
            top: 0.25rem;
            left: 0.15rem;
        }
        input[src$="Go.gif"] {
            position: relative;
            top: 0.20rem;
            left: -0.15rem;
        }
        $onMobile {
            input[name=q] {
                margin-top: 0.3rem;
                max-width: 55vw;
            }

            body
            > table:nth-of-type(2)
            > tbody
            > tr:first-of-type
            > td:first-of-type
            > div:first-of-type
            {
                display: inline !important;
                text-align: center;
            }

            body
            > table:nth-of-type(2)
            > tbody:first-of-type
            > tr:first-of-type
            > td:first-of-type
            {
                max-width: 15rem;
            }
        }


        /* The banner right of the search form. */
        $banners {
            color: $darkGray;
            font-weight: bold;
        }
        $onDesktop {
            $banners {
                font-size: 175%;
            }
        }
        $onMobile {
            $banners {
                font-size: 125%;
            }

            body
            > table:nth-of-type(2)
            > tbody:first-of-type
            > tr:first-of-type
            > td:nth-of-type(2)
            {
                text-align: left;
            }
        }


        /* sidefloat */
        .sidebox {
            border: solid $borderWidth $lightGray;
            padding: 0.5rem;
            margin-bottom: 1rem;
        }
        .sidebox > * {
            margin: 0.2rem;
            padding: 0.2rem;
        }
        $onDesktop {
            .sidefloat {
                float: right;
                width: 25rem;
                margin-left: 1.5rem;
            }
        }
        $onMobile {
            .sidefloat {
                display: flex;
                flex-wrap: wrap;
                justify-content: space-between;
                margin-left: -0.5rem;
                margin-right: -0.5rem;
            }
            .sidebox {
                flex-basis: 15rem;
                flex-grow: 1;
                margin-left: 0.5rem;
                margin-right: 0.5rem;
            }
        }


        /* The footer. */
        .footer {
            font-size: 75%;
            line-height: 1.5;
            padding-bottom: 2rem;
        }
        .footer > small:after {
            content:"\a";
            white-space: pre;
        }


        /* Page-specific rules. */

        /* Download page: the downloads table. */
        #downloads {
            margin: 0 auto;
        }
        $onDesktop {
            #downloads {
                max-width: 60rem;
            }
        }
        $onMobile {
            #downloads {
                font-size: 75%;
                max-width: 80vw;
            }
        }
        #downloads th {
            border-bottom: solid $borderWidth $gray;
        }
        #downloads td, #downloads th {
            margin: 0;
            padding: .5rem;
            text-align: left;
            vertical-align: top;
        }
        #downloads tr.odd td,tr.odd th{
            background: $bgWhite;
        }
        #downloads tr.even td,tr.even th{
            background: $tableAltGray;
        }


        /* Front page: header in a blurb. */
        .homesectionhead {
            font-weight: bold;
        }


        /* Conferences page. */
        .conferences table {
            font-size: 75%;
        }
    }]
}

proc unindent {
    text
    {chars { }}
    {ignoreIndentOnlyLines true}
    {max inf}
} {
    regsub ^\n $text {} text
    if {$ignoreIndentOnlyLines} {
        regsub \n\[$chars\]*?$ $text {} text
    } else {
        regsub \n$ $text {} text
    }

    set rLeading ^\[$chars\]*
    set rBlankLine $rLeading$

    foreach line [split $text \n] {
        if {$line eq {}
            || ($ignoreIndentOnlyLines
                && [regexp $rBlankLine $line])} continue

        regexp -indices $rLeading $line idc
        set count [expr {[lindex $idc 1] + 1}]

        set max [expr {min($max,$count)}]
    }

    set start [expr { $max == inf ? {end+1} : $max }]

    join [lmap line [split $text \n] {
        string range $line $start end
    }] \n
}

# If this is the main script...
if {[info exists argv0] && ([file tail [info script]] eq [file tail $argv0])} {
    fconfigure stdout -encoding utf-8
    puts [gen]
}
