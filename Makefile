all: devsite.min.css

devsite.min.css: devsite.css
	minify -o $@ $<

devsite.css: css.tcl vendor/normalize.css/normalize.css
	cp vendor/normalize.css/normalize.css $@
	tclsh $< >> $@

.PHONY: all
